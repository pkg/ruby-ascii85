ruby-ascii85 (1.0.3-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 02:04:36 +0000

ruby-ascii85 (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3
  * Run wrap-and-sort on packaging files
  * Move debian/watch to gemwatch.debian.net
  * Bump Standards-Version to 4.1.5 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * README is now markdown
  * Update upstream email address

 -- Cédric Boutillier <boutil@debian.org>  Mon, 30 Jul 2018 22:32:40 +0200

ruby-ascii85 (1.0.2-3) unstable; urgency=medium

  * Reupload to update rubygems-integration metadata
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Set Testsuite to autopkgtest-pkg-ruby
  * Change homepage to the page of the Github repository
  * Refresh remove_rubygems.patch to remove the relative path when requiring
    the library in tests

 -- Cédric Boutillier <boutil@debian.org>  Tue, 14 Oct 2014 23:45:13 +0200

ruby-ascii85 (1.0.2-2) unstable; urgency=low

  * debian/patches: add fix_path_in_bin.patch to use the correct path to find
    the ascii85 library. Thanks Sam Morris (Closes: #710006).

 -- Cédric Boutillier <boutil@debian.org>  Mon, 27 May 2013 23:22:29 +0200

ruby-ascii85 (1.0.2-1) unstable; urgency=low

  * New upstream version
  * debian/control: 
    + remove obsolete DM-Upload-Allowed flag
    + use canonical URI in Vcs-* fields
    + bump Standards-Version to 3.9.4 (no changes needed)
    + change in Build-Depends ruby-rspec to ruby-minitest to reflect upstream
      test framework change
    + update my email address
  * debian/copyright:
    + use DEP5 copyright-format/1.0 official URL for Format field
    + update my email address and years
  * debian/ruby-test-files.yaml: drop spec_helper.rb, not present any more
  * debian/patches:
    + drop 01_spec_fix_requires.diff, not needed anymore
    + add remove_rubygems.patch

 -- Cédric Boutillier <boutil@debian.org>  Sun, 19 May 2013 21:02:48 +0200

ruby-ascii85 (1.0.1-2) unstable; urgency=low

  * Bump build dependency on gem2deb to >= 0.3.0~

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Tue, 26 Jun 2012 07:39:47 +0200

ruby-ascii85 (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #640110)

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Fri, 02 Sep 2011 22:34:48 +0200
